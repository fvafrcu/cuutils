Dear CRAN Team,
this is a resubmission of package 'cuutils'. I have added the following changes:

* FIXME

Please upload to CRAN.
Best, Andreas Dominik

# Package cuutils 0.2.0.9000

Reporting is done by packager version 1.10.0.9002


## Test environments
- R Under development (unstable) (2021-01-20 r79849)
   Platform: x86_64-pc-linux-gnu (64-bit)
   Running under: Devuan GNU/Linux 3 (beowulf)
   0 errors | 1 warning  | 0 notes
- gitlab.com
  R version 4.0.3 (2020-10-10)
  Platform: x86_64-pc-linux-gnu (64-bit)
  Running under: Ubuntu 20.04 LTS
  Status: 3 NOTEs

- win-builder (devel)

## Local test results
- RUnit:
    cuutils_unit_test - 3 test functions, 0 errors, 0 failures in 6 checks.
- Testthat:
    
- Coverage by covr:
    cuutils Coverage: 11.29%

## Local meta results
- Cyclocomp:
     Exceeding maximum cyclomatic complexity of 10 for install_cran by 6.
- lintr:
    found 32 lints in 674 lines of code (a ratio of 0.0475).
- cleanr:
    found 1 dreadful things about your code.
- codetools::checkUsagePackage:
    found 5 issues.
- devtools::spell_check:
    found 3 unkown words.

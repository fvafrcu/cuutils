---
output: github_document
---
[![pipeline status](https://gitlab.com/fvafrCU/cuutils/badges/master/pipeline.svg)](https://gitlab.com/fvafrCU/cuutils/commits/master)    
[![coverage report](https://gitlab.com/fvafrCU/cuutils/badges/master/coverage.svg)](https://gitlab.com/fvafrCU/cuutils/commits/master)
<!-- 
    [![Build Status](https://travis-ci.org/fvafrCU/cuutils.svg?branch=master)](https://travis-ci.org/fvafrCU/cuutils)
    [![Coverage Status](https://codecov.io/github/fvafrCU/cuutils/coverage.svg?branch=master)](https://codecov.io/github/fvafrCU/cuutils?branch=master)
-->
[![CRAN_Status_Badge](https://www.r-pkg.org/badges/version/cuutils)](https://cran.r-project.org/package=cuutils)
[![RStudio_downloads_monthly](https://cranlogs.r-pkg.org/badges/cuutils)](https://cran.r-project.org/package=cuutils)
[![RStudio_downloads_total](https://cranlogs.r-pkg.org/badges/grand-total/cuutils)](https://cran.r-project.org/package=cuutils)

<!-- README.md is generated from README.Rmd. Please edit that file -->



# cuutils
## Introduction
After installation, read the help page:

```r
help("cuutils-package", package = "cuutils")
```

```
#> What it Does (One Line, Title Case)
#> 
#> Description:
#> 
#>      A description is a paragraph consisting of one or more sentences.
#> 
#> Details:
#> 
#>      You will find the details in
#>      'vignette("An_Introduction_to_cuutils", package = "cuutils")'.
```

## Installation

You can install cuutils from gitlab with:


```r
if (! require("remotes")) install.packages("remotes")
remotes::install_gitlab("fvafrCU/cuutils")
```



#' Does an Object Exist and \code{\link{isTRUE}}?
#'
#' \code{\link{isTRUE}} gives an error, if \code{x} does not exists. This
#' function gives FALSE in this case.
#' @param obj The object to be checked for.
#' @return TRUE if the \code{obj} exists and is TRUE.
#' @export
is_TRUE <- function(obj) {
    res <- FALSE
    if (exists(deparse(substitute(obj)))) {
        res <- isTRUE(obj)
    }
    return(res)
}

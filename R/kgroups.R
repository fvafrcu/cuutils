#' Determine Subsizes Close to Equality
#'
#' Determine the sizes of k subsets of a set with
#' n elements in such a way that the sizes are as
#' equal as possible.
#'
#' @param n The size of the set.
#' @param k The number of subsets.
#' @return A vector of k subsizes.
#' @export
#' @examples
#' subsizes(n = 100, k = 6)
#' subsizes(n = 2, k = 6)
subsizes <- function(n, k){
    if (k > n) {
        warning("Got k > n, this will result in subsizes of 1 and 0.")
        res <- c(rep(1, n), rep(0, k - n))
    } else {
        if (n %% k == 0) {
            res <- rep(n / k, k)
        }
        else {
        fl <- floor(n / k)
        ce <- ceiling(n / k)
            res <- c(rep(ce, round((n / k - fl) * k))
                     , rep(fl, round((1 - (n / k - fl)) * k)))
        }
    }
    return(res)
}

#' Determine Indices and Sizes of Subsets
#'
#' @param n The size of the set.
#' @param k The number of subsets.
#' @return A matrix with starting index, size, and stopping index for each
#' subset.
#' @export
#' @examples
#' kgroups(n = 100, k = 6)
#' kgroups(n = 2, k = 6)
kgroups <- function(n, k) {
    size <- subsizes(n, k)
    start <- 1 + c(0, cumsum(size)[1:(k-1)])
    stop <- start + size - 1
    res <- cbind(id = 1:k, start, size, stop)[size > 0, TRUE]
    return(res)

}


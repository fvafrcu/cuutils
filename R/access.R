#' Read an MS Access Table into R
#'
#' @param path_to_db The path to the MS Access database file.
#' @param table_name The MS Access table name.
#' @param lowernames Convert names to lowercase?
#' @param auto_assign Automatically assign the data frame to the current working
#' directory?
#' @return a data frame, or, if auto_assign = TRUE,
#' \code{\link[base:invisible]{invisibly}} \code{\link{NULL}}.
#' @export
read_ms_access_table <- function(path_to_db, table_name, lowernames = TRUE,
                      auto_assign = FALSE) {
    if (isTRUE(lowernames)) {
        name <- tolower(table_name)
    } else {
        name <- table_name
    }

    if (fritools::is_windows()) {
        if (! is_32bit()) stop("MS Access stuff runs 32 bit only!")
        tmp <- RODBC::odbcConnectAccess2007(path_to_db)
        table <- RODBC::sqlFetch(tmp, table_name)
        RODBC::odbcClose(tmp)
        if (isTRUE(lowernames)) names(table) <- tolower(names(table))
    } else {
        table <- Hmisc::mdb.get(allow = c("_"), path_to_db, tables = table_name,
                                lowernames = lowernames,
                                mdbexportArgs='-b raw')


    }
    if (isTRUE(auto_assign)) {
        assign(name, table, pos = parent.env(environment()))
        return(invisible(NULL))
    } else {
        return(table)
    }

}


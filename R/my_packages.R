#' Get Names of Sets of my Packages
#'
#' @param type Give the set of packages:
#' \itemize{
#'  \item{"cran"}{CRAN packages only}
#'  \item{"fvafrcu"}{Non-CRAN packages only}
#'  \item{"devel"}{union of "cran" and "fvafrcu"}
#'  \item{"bioc"}{"devel" plus bioconductor dependent ones}
#' }
#' @return A Character vector of packages names.
#' @export
#' @examples
#' cuutils::get_package_names()
#' cuutils::get_package_names("fvafrcu")
#' cuutils::get_package_names("devel")
#' cuutils::get_package_names("bioc")
get_package_names <- function(type = c("cran", "fvafrcu", "devel", "bioc")) {
    checkmate::assert_character(type)
    type <- match.arg(type)
    cran_packages <- c("HandTill2001", "maSAE", "document", "excerptr", 
                       "cleanr", "rasciidoc", "fakemake", "packager")
    fvafrcu_packages <- c("cuutils", "bundeswaldinventur", "rbdatpro", 
                          "regulaFalsi")
    bioc_dependent <- c("cptables")
    if (identical(.Platform[["OS.type"]], "windows")) 
        fvafrcu_packages <- c(fvafrcu_packages, "wehamr")

    
    package_names <- switch(type,
                            "cran" = cran_packages,
                            "fvafrcu" = fvafrcu_packages,
                            "devel" = c(cran_packages, fvafrcu_packages),
                            "bioc" = c(cran_packages, fvafrcu_packages, 
                                       bioc_dependent),
                            throw(paste0("Got unkown type ", type, ".")))

    return(package_names)
}


#' Install Sets of my Packages
#' 
#' @inheritParams get_package_names
#' @return \code{\link[base:invisible]{Invisibly}} the return value of  
#' \code{\link[remotes:install_cran]{remotes::install_cran}}.
#' @export
#' @examples
#' \dontrun{
#' print(cuutils::install_packages())
#' print(cuutils::install_packages("fvafrcu"))
#' }
install_packages <- function(type = c("cran", "fvafrcu", "devel", "bioc")) {
    checkmate::assert_character(type)
    type <- match.arg(type)
    names <- get_package_names(type)
    res <- switch(type,
                  "cran" = remotes::install_cran(names),
                  "fvafrcu" = ,
                  "devel" = remotes::install_gitlab(paste0("fvafrcu/", names)),
                  throw(paste0("Got unkown type ", type, ".")))
    return(invisible(res))
}

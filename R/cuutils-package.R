#' What it Does (One Line, Title Case)
#'
#' A description is a paragraph consisting of one or more sentences.
#'
#' You will find the details in\cr \code{vignette("An_Introduction_to_cuutils",
#' package = "cuutils")}.
#'
#' @name cuutils-package
#' @aliases cuutils-package
#' @docType package
#' @keywords package
NULL

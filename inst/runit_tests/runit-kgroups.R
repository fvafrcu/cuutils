test_subsizes <- function() {
    expectation <- rep(20, 5)
    result <- subsizes(n = 100, k = 5)
    RUnit::checkEquals(result, expectation)
    expectation <- c(rep(17, 4), rep(16, 2))
    result <- subsizes(n = 100, k = 6)
    RUnit::checkEquals(result, expectation)
    expectation <- c(rep(1, 2), rep(0, 4))
    result <- subsizes(n = 2, k = 6)
    RUnit::checkEquals(result, expectation)
}

test_kgroups <- function() {
    expectation <- structure(c(1, 2, 3, 4, 5, 6, 1, 18, 35, 52, 69, 85, 17, 17,
                               17, 17, 16, 16, 17, 34, 51, 68, 84, 100), 
                             .Dim = c(6L, 4L), 
                             .Dimnames = list( NULL, c("id", "start", "size",
                                                       "stop")))
    result <- kgroups(n = 100, k = 6)
    RUnit::checkEquals(result, expectation)
    expectation <- structure(c(1, 2, 1, 2, 1, 1, 1, 2), 
                             .Dim = c(2L, 4L), 
                             .Dimnames = list(NULL, c("id", "start", "size",
                                                      "stop")))
    result <- kgroups(n = 2, k = 6)
    RUnit::checkEquals(result, expectation)
}
